https://spring.io/guides/gs/centralized-configuration/
https://cloud.spring.io/spring-cloud-config/

config server
	maven
		<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-config-server</artifactId>
		</dependency>  ....<dependencyManagement><dependencies><dependency><artifactId>spring-cloud-dependencies</artifactId>
	
	@EnableConfigServer,@SpringBootApplication
	git -> application.properties -> a-bootiful-client.properties where spring.application.name=a-bootiful-client
	application.properties > spring.cloud.config.server.git.uri=${HOME}/Desktop/config, server.port=8888


config client
	maven
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>.......<artifactId>spring-cloud-dependencies
	spring.application.name=a-bootiful-client
	spring.cloud.config.uri=http://localhost:8888 #default
	
	...use @Value("${message:Hello default}") or @ConfigurationProperties(prefix='') class->member....