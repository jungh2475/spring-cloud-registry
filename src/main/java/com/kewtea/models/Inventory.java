package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "inventory")
public class Inventory {
/*
	compute_instance(gcloud, aws, vbox, docker), iot_node_instance, storage_instance
	user
	id
	name, type_hw, type_sw, tag (version), group, description, 
	network_ip_public, network_ip_prvate, network_ports
	sw_installed
	instance_owner, project
	createdWhen, timezone
	region(location), region_encoding_format
	readAccess
	writeAccess
	
*/	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	
	//@Column(columnDefinition="tinyint(1) default 1")
	
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int created_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long created_timestamp;
	
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int updated_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long updated_timestamp;
	
	@NotEmpty
	@Column(nullable=false)
	public String type; // like title: type: compute_instance, iot_node, iot_sensor
	@Column(nullable=true)
	public String subclass;  //class-등급 
	
	@Column(nullable = true)
	public long projectid;
	@Column(nullable = true)
	public long parentid;
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean draft;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean snapshot;
	
	@Column(nullable=true)
	public String modelVersion;
	
	@Column(nullable = true)
	public long createdby;
	
	
	@Column(nullable=true)
	public String name;
	@Column(nullable=true)
	public String uniquename;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
	
	
	
}
