package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "tags")
public class Tag {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	
	@Column(nullable = true)
	public int created_tzone; 
	@Column(nullable = true)
	public long created_timestamp; 
	@Column(nullable = true)
	public long created_by;  //who=uid?
	
	@Column(nullable = true)
	public int updated_tzone; 
	@Column(nullable = true)
	public long updated_timestamp;
	
	@Column(nullable = true)
	public long parent_id;
	@Column(nullable = true)
	public String type;  //? user_collection, user_keyword, user_tag, taxanomy_word_meaning....
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean snapshot;
	
	@Column(nullable = true)
	public String locale;  //region-language
	@Column(nullable = true)
	public long uid;
	
	
	@Column(nullable = true)
	public long word_id;  //meaning of the word, from taxanomy-tree, 
	@Column(nullable = true)
	public String name;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
}
