package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "snapshots")
public class Snapshot {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty
	@Column(nullable = false)
	public int created_tzone=1;;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long created_timestamp;
	
	@Column(nullable = true)
	public int lastupdated_tzone=1;;
	
	@Column(nullable = true)
	public long lastupdated_timestamp;
	
	@NotEmpty
	@Column(nullable=false)
	public String table_name;  //이것이 제일 중요함, 어떤 model을 snapshot한것인가?
	
	@NotEmpty
	@Column(nullable = false)
	public long itemid;  // item id 이것이 제일 중요함. 
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	
	@Column(nullable=true)
	public String modelVersion;
	
	
}
