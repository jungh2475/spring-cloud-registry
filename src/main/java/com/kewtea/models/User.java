package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "users")
public class User {
	
	
	//type: user, userGroup, serviceAccount(machine), instanceuser(within machine)
	
	//password
	//platform (serviceName or instanceName)
	
	//readRight, writeRight=owners
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int created_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	@NotEmpty
	@Column(nullable = false)
	public long created_timestamp;
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int updated_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	@NotEmpty
	@Column(nullable = false)
	public long updated_timestamp;
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean snapshot;
	
	@Column(nullable = true)
	public String type;  //(end)user, userGroup, serviceAccount, instanceAccount, app_instanceAccount
	@Column(nullable = true)
	public String ref_id;  //user_id, app_id or instance_id, app_instance_id
	
	@Column(nullable = true)
	public String username;
	@Column(nullable = true)
	public String usergroup;
	@Column(nullable = true)
	public String password;
	@Column(nullable = true)
	public String accesstoken;
	@Column(nullable = true)
	public String useremail;
	@Column(nullable = true)
	public String scope;
	@Column(nullable = true)
	public String locale;  //country-language
	
	@Column(nullable = true)
	public String userhomedir;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
	

}
