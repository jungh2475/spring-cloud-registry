package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

/*
 *  SysRecord1 => mysql record
 *  SysRecord2 => elastic record
 * 	
 * 		long_id
 * 		timestamp,tzone, type, tag, message, reference_int
 * 		reporter_source(machine, application, service), 
 */

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "sysrecord")
public class SysRecord {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	
	//@Column(columnDefinition="tinyint(1) default 1")
	
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int org_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long org_timestamp;  //origin_timestamp, not created/updated here
	
	@Column(nullable = true)
	public long did;  // device id, machine id, instance id, optional
	
	@Column(nullable = true)
	public long aid;  // application id or service id(journal, spot..), mandatory for client app 
	
	@Column(nullable = true)
	public long sid;   // service id
	
	@NotEmpty
	@Column(nullable=false)
	public String level;  //log level: e, i, w, d(developer)
	
	@NotEmpty
	@Column(nullable=false)
	public String type; // like title: type: client-ip, cookie(expire,authority-or-role), clientid(=deviceid+ serviceid-or-swappid)	
	
	@Column(nullable = true)
	public String tag; //like keywords
	
	@NotEmpty
	@Column(nullable = false)
	public String msg;  //value here
	
	@Column(nullable = true)
	public int ref_int;  //value here or double
	
	@Column(nullable = true)
	public String ref_str;  //value here
	

	@Column(nullable = true)
	public long uid;
	
	public SysRecord(String level, String type, String msg, int timezone, int timestamp ){
		this.level=level; this.type=type; this.msg=msg; this.org_tzone=timezone; this.org_timestamp=timestamp;
	}
	
	public SysRecord(long uid, long aid, String level, String type, String msg, int timezone, int timestamp ){
		this.uid=uid; this.aid=aid; this.level=level; this.type=type; this.msg=msg; this.org_tzone=timezone; this.org_timestamp=timestamp;
	}
	
	
}
