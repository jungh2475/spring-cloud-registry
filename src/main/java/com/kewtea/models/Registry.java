package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor  //@NoArgsConstructor
@Table(name = "registry")
public class Registry {
	/*
	 * 
	 	about_project, parameter as key_value
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int created_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long created_timestamp;
	
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int updated_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	
	@NotEmpty
	@Column(nullable = false)
	public long updated_timestamp;
	
	@NotEmpty
	@Column(nullable=false)
	public String type; // project, config, parameter
	@Column(nullable=true)
	public String subclass;  
	
	@Column(nullable = true)
	public long projectid;
	@Column(nullable = true)
	public long parentid;
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean draft;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean snapshot;
	
	
	@Column(nullable=true)
	public String readAccess;
	@Column(nullable=true)
	public String writeAccess;
	@Column(nullable = true)
	public long createdby;
	
	@Column(nullable=true)
	public String key;
	@Column(nullable=true)
	public String uniquekey;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
	

}
