package com.kewtea.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

/*
	id
	name, type(crawl,action,test), parentTask, group, description, 
	due, function(id), input(json or url or object), output(sysConn,db,email,url-rest-or-file), 
	status(draft/open/closed/obsolete/completed), assignedTo(instanceId,appId), assignedWhen
	createdWhen, timezone
*/

@Entity
@Data
@Table(name = "cloudcontents")
public class ComputeContent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int created_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	@NotEmpty
	@Column(nullable = false)
	public long created_timestamp;
	@NotEmpty
	@Column(nullable = false)  //안적혀져 있으면 server timezone 적용 
	public int updated_tzone=1;  //+9, -8, e.g. Asia/Seoul, Europe/London, Etc/GMT+6  check http://tutorials.jenkov.com/java-date-time/java-util-timezone.html
	@NotEmpty
	@Column(nullable = false)
	public long updated_timestamp;
	
	@NotEmpty
	@Column(nullable=false)
	public String type; //type: task-template(작업 내용 표시),
	@Column(nullable=true)
	public String subclass;  //class-등급 
	
	@Column(nullable = true)
	public long projectid;
	@Column(nullable = true)
	public long parentid;
	
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)  //0 is false
	public boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean flag;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean draft;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public boolean snapshot;
	
	@Column(nullable=true)
	public String readAccess;
	@Column(nullable=true)
	public String writeAccess;
	@Column(nullable = true)
	public long createdby;
	
	@Column(nullable=true)
	public String name;
	@Column(nullable=true)
	public String uniquename;
	
	@Lob
	@Column(columnDefinition = "TEXT")
	public String body;
			 
	
}
