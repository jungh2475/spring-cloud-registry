package com.kewtea.controllers;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kewtea.models.SysRecord;

@Controller
@RequestMapping("/api/registry")
public class RegistryRestController {
	
	private static final Logger log = Logger.getLogger(RegistryRestController.class.getName());
	
	//@Autowired
	//service
	//private functions
	
	@RequestMapping(value = "/report/", method=RequestMethod.POST)  //with valid api key
	@ResponseStatus(HttpStatus.CREATED)
	public SysRecord empty_method(){
		SysRecord sysRecord=new SysRecord(null, null, null, 0, 0);
		return sysRecord;
	}
	
	
	
	
}
