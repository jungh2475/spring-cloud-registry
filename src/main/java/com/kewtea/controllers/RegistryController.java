package com.kewtea.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/registry")
public class RegistryController {
	
	@RequestMapping(value={"","/","/test"}, method = RequestMethod.GET)
	String home1(){
		
		return "kewtea/about";
	}
	
}
