



######## install 
sudo apt-get install python-pip
sudo pip install apache-libcloud  # pip install apache-libcloud==1.5.0



####### AWS
http://www.pixelite.co.nz/article/using-apache-libcloud-provision-cloud-servers/

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

cls = get_driver(Provider.EC2_AP_SOUTHEAST2)
driver = cls(AWS_EC2_ACCESS_ID, AWS_EC2_SECRET_KEY)

ACCESS_KEY_NAME = 'provision'

SECURITY_GROUP_NAMES = []
SECURITY_GROUP_NAMES.append('ssh_access')

MY_SIZE = 'm1.small'
MY_IMAGE = 'ami-934ddea9'

sizes = driver.list_sizes()
images = driver.list_images()

size = [s for s in sizes if s.id == MY_SIZE][0]
image = [i for i in images if i.id == MY_IMAGE][0]

print size
print image

node = driver.create_node(name="My Instance", image=image, size=size,
ex_keyname=ACCESS_KEY_NAME, ex_securitygroup=SECURITY_GROUP_NAMES)

print node

#ssh -l root ec2-54-252-210-134.ap-southeast-2.compute.amazonaws.com



####### Google -compute, storage
https://libcloud.apache.org/blog/2014/02/18/libcloud-0-14-and-google-cloud-platform.html
https://github.com/apache/libcloud/blob/trunk/demos/gce_demo.py



from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

Driver = get_driver(Provider.GCE)
gce = Driver('your_service_account_email', 'path_to_pem_file',
             datacenter='us-central1-a',  project='your_project_id')

             
sizes = gce.list_sizes()
images = gce.list_images()

size_obj = [s for s in sizes if s.id == 'n1-standard-1'][0]
image_obj = [i for i in images if i.name == 'debian-7'][0]

new_node = gce.create_node(name='my_node', size='n1-standard-1', image='debian-7')
#new_node = gce.create_node(name='my_node', size=size_obj, image=img_obj)