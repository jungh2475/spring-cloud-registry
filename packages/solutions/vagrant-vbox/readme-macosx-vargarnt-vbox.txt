vbox(oracle virtual box)
http://www.howopensource.com/2011/06/how-to-use-virtualbox-in-terminal-commandline/
http://superuser.com/questions/584100/why-should-i-use-vagrant-instead-of-just-virtualbox

VBoxManage list vms
VBoxManage list runningvms
VBoxManage showvminfo avahi_test01 | grep MAC

VBoxManage createvm --name Ubuntu10.10 --register

VBoxManage createvm --name Ubuntu10.10
VBoxManage createhd --filename Ubuntu10.10 --size 5120
VBoxManage registervm '/home/user/VirtualBox VMs/Ubuntu10.10/Ubuntu10.10.vbox'

VBoxManage modifyvm Ubuntu10.10 --ostype Ubuntu (--memory 512)
VBoxManage storagectl Ubuntu10.10 --name IDE --add ide --controller PIIX4 --bootable on
VBoxManage storagectl Ubuntu10.10 --name SATA --add sata --controller IntelAhci --bootable on
VBoxManage storageattach Ubuntu10.10 --storagectl SATA --port 0 --device 0 --type hdd --medium "filename"
VBoxManage storageattach Ubuntu10.10 --storagectl IDE --port 0 --device 0 --type dvddrive --medium "filename"
VBoxManage modifyvm Ubuntu10.10 --vram 128 --accelerate3d on --audio alsa --audiocontroller ac97
VBoxManage modifyvm Ubuntu10.10 --nic1 nat --nictype1 82540EM --cableconnected1 on

VBoxManage startvm Ubuntu10.10
VBoxManage startvm Ubuntu10.10 --type headless
VBoxHeadless --startvm Ubuntu10.10

VBoxManage modifyvm Ubuntu10.10 --vrde on --vrdeport 5012 --vrdeaddress 192.168.1.6 

VBoxManage controlvm Ubuntu10.10 poweroff

-------
vagrant
(import pre-made images-boxes, setting, scripting for vbox)
download and install from https://www.vagrantup.com/
mkdir cd to your dev directroy
vagrant init hashicorp/precise64 (vbox Ubuntu 12.04 LTS 64-bit)
vagrant up
(ssh connection)vagrant ssh
vagrant destroy (for stop &shutdown, vagrant halt)
vagrant global-status
vagrant halt -f 6b855b0
cat Vagrantfile
ps aux | grep -i vbox
vagrant box list ->vagrant box remove/add NAME
vagrant status (within Vagrantfile directory)

vagrant package --output mynew.box (snapshot copy)
vagrant box add mynewbox mynew.box
vagrant init mynewbox

------------------------
sudo apt-get clean
sudo dd if=/dev/zero of=/EMPTY bs=1M #only for ubuntu
sudo rm -f /EMPTY #only for ubuntu
cat /dev/null > ~/.bash_history && history -c && exit

