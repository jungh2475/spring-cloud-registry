#! /usr/bin/env sh
 
### chmod +x then run test as ./sample_est.sh ###
 
function testWeCanWriteTests () {
	
	#load config file
	#load filescript as function then compare outputs of interest with expected values
	
	
    assertEquals "it works" "it works"
}

#fileoutput으로 비교 
function testItCanProvideAllTheScores () {
    cd ..
    ./tennisGame.sh ./input.txt > ./results.txt
    diff ./output.txt ./results.txt
    #checks the exit value of diff
    assertTrue 'Expected output differs.' $? 
    
}

#testing function defined in file
source ../functions.sh
 
function testItCanProvideFirstPlayersName () {
    assertEquals 'John' `getFirstPlayerFrom 'John - Michael'`
}

 
## Call and Run all Tests
. "../shunit2-2.1.6/src/shunit2"


#### function under test
function num_chars {
  echo "${1}" | wc -c
}

local res=$(num_chars "foo")  #$res=3,, ${res} , In most cases, $var and ${var} are the same
