#!/bin/bash
# example use: util-list-vars your-script.sh > vars.files.save
#( set -o posix ; set ) | less

( set -o posix ; set ) >/tmp/variables.before
source $1
( set -o posix ; set ) >/tmp/variables.after
echo "showing the variables in the script"
diff /tmp/variables.before /tmp/variables.after
#read -p "continue(enter?)" Y
rm /tmp/variables.before /tmp/variables.after


#set -a
#env > /tmp/a
#source $1
#env > /tmp/b
#diff /tmp/{a,b} | sed -ne 's/^> //p'