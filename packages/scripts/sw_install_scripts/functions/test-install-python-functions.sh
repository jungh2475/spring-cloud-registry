#!/bin/bash

##########  2/3  sub_install packages as function    #############

STEP1="1/6  python3 and pip install"
function install_python3_pip {
	sudo apt-get update
	sudo apt-get upgrade
	sudo apt-get install python-software-properties
	sudo apt-get install software-properties-common
	sudo apt-get -y install git
	
	#install oracle java
	sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get install oracle-java8-installer
	sudo update-alternatives --config java #list all java installations
	echo #####JAVA_HOME="/usr/lib/jvm/java-8-oracle"
	read -p 'edit /etc/environment' YESNO
	sudo nano /etc/environment
	source /etc/environment
	
	sudo apt-get -y install jq
	sudo apt-get -y install curl
	
	#check python3
	python3 --version
	which python3 #As of Ubuntu 16.04 LTS, Python 3 is default Python!
	
	
	
}





STEP1="2/6 python-ml-project-git install"
function install_pygit_ml_project {
	cd $PROJECTDIR
	mkdir $PYMLPROJECTDIR
	cd $PYMLPROJECTDIR
	git init
	git remote add origin $PYMLGIT
	git pull origin master
	
	
	echo 'virtualenv at venv1 folder' #pyvenv venv1 필요없음 # venv1안에 git ignore 필요
	##set PYTHONPATH
	
	cd $PROJECTDIR'/'$PYMLPROJECTDIR
	source venv1/bin/activate #deactivate
	echo 'installing numpy,pandas,scikit-learn using requirements.txt
	pip install -r requirements.txt #or pip3 install...mac에서 문제가 있으면 sudo xcode-select --install
	pip freeze #list the python modules installed	
	
	#install firefox, chrominum, phantomjs
	cd ~
	sudo apt-get update
	sudo apt-get install firefox #sudo apt-get purge firefox, mv ~/.mozilla ~/mozilla-backup
	sudo apt-get install chromium-browser
	#install phantomjs: http://phantomjs.org/download.html
	sudo apt-get install phantomjs
	
	#sudo apt-get install build-essential chrpath libssl-dev libxft-dev
	#sudo apt-get install libfreetype6 libfreetype6-dev
	#sudo apt-get install libfontconfig1 libfontconfig1-dev
	#export PHANTOM_JS="phantomjs-2.1.1-linux-x86_64" # phantomjs-1.9.8-linux-x86_64
	#wget https://bitbucket.org/ariya/phantomjs/downloads/$PHANTOM_JS.tar.bz2
	#https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
	#sudo tar xvjf $PHANTOM_JS.tar.bz2
	#sudo mv $PHANTOM_JS /usr/local/share
	#sudo ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin
	phantomjs --version
	
}

STEP1="3/6 mysql install"
function install_mysql_server {
	
	echo "running .....install_mysql_server at local for testing, and make sure you use remote origin db server..."
	sudo apt-get -y install mysql-server
	# mysql_secure_installation ; which removes the test database and secures the server for production.
	sudo mysql_secure_installation
	which mysql
	mysql --version
	read -p "test remote mysql connection with your account details" YESNO
	if [ $YESNO="Y" ] 
	then
		echo "you must explicitly allow access from this machine to main db server because secure_install did such DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
		echo "GRANT ALL PRIVILEGES ON * . * TO '$MYSQLUSERNAME'@'$HOSTIPPRIVATE';"
		echo 'host:'$MYSQLHOSTIP ',db:'$MYSQLDBNAME ',u:'$MYSQLUSERNAME ',p:'$MYSQLUSERPASSWORD
		echo "you selected "$YESNO
	fi
	mysql -h $MYSQLHOSTIP -D $MYSQLDBNAME -u $MYSQLUSERNAME -p$MYSQLUSERPASSWORD -e "use $MYSQLDBNAME; show tables; quit;"
	#remote mysql 서버가 현재 서버에서 접근하는 것을 허락하도록 해야한다 
	#만약에 현재 서버가 MYSQL MAIN 서버라면 아래의 등록은 필요가 없다 
	read -p "type such in remote mysql ......""GRANT ALL PRIVILEGES ON * . * TO '$MYSQLUSERNAME'@'$HOSTIPPRIVATE'; FLUSH PRIVILEGES;(YESNO)" YESNO
	sudo mysql -h $MYSQLHOSTIP -u $MYSQLUSERNAME -p$MYSQLUSERPASSWORD
	
	#mysql -h '127.0.0.1' -D kew1db -u root -p1234 -e "use kew1db; show tables;"
	#mysql -h $MYSQLHOSTIP -D -u -p -e "quit;"	
	# mysql -h [ip] -D [db name] -u [user] -p[pass] -e "[mysql commands]"
	# mysql -u [user] -p[pass] << EOF
	#[mysql commands]
	#EOF
	
	#set localdb user with password (로컬에 테스트용 mysql에 할당 시키고, 리모트의 원래 mysql은 계정 정보 가져와서 알아서 적읍시다 )
	
	echo "mysql>CREATE DATABASE `$MYSQLDBNAME` CHARACTER SET utf8 COLLATE utf8_general_ci;"
	echo "mysql>CREATE USER '$MYSQLUSERNAME'@'localhost' IDENTIFIED BY '$MYSQLUSERPASSWORD';"
	echo "mysql>GRANT ALL PRIVILEGES ON * . * TO '$MYSQLUSERNAME'@'localhost';"
	echo "FLUSH PRIVILEGES;"
	read -p "please login mysql and run mysql_secure_installation and add users" YESNO
	sudo mysql -u root -p$MYSQLPASSWORD
	
	#sudo mysql -u root -p$MYSQLPASSWORD << EOF
	#CREATE DATABASE `$MYSQLDBNAME` CHARACTER SET utf8 COLLATE utf8_general_ci;
	#CREATE USER '$MYSQLUSERNAME'@'localhost' IDENTIFIED BY '$MYSQLUSERPASSWORD';
	#GRANT ALL PRIVILEGES ON * . * TO '$MYSQLUSERNAME'@'localhost';
	#FLUSH PRIVILEGES;
	#EOF
	#set dbname with utf-8
	#GRANT [type of permission] ON [database name].[table name] TO ‘[username]’@'localhost’;
	#반대는 REVOKE,....사용자 삭제는 DROP USER ‘demo’@‘localhost’;,....
	
	
	TEMPSTR="mysql-local userName:"$MYSQLUSERNAME",password:"$MYSQLUSERPASSWORD
	echo TEMPSTR
	lgo TEMPSTR
	
	#restart mysql server
	sudo systemctl enable mysqld
	sudo systemctl start mysqld

}

STEP1="4/6 elk install"
function install_elk {
	echo "running .....install_elk(elasticsearch)"
	echo 'installation is at '$ELATIC_INSTALL_PATH
	#cd $ELK_INSTALL_PATH #설치 디렉토리로 이동  
	# 2.sudo update
	sudo apt-get update
	# 1.register repository for debian/ubuntu : https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html
	#download public signing key:4609 5ACC 8548 582C 1A26 99A9 D27D 666C D88E 42B4 
	wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
	sudo apt-get install apt-transport-https
	echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list
	
	
	# 3.install elastic
	sudo apt-get update && sudo apt-get install elasticsearch
	nano /etc/elasticsearch/elasticsearch.yml #optional
	# ES_HOME=/usr/share/elasticsearch, path.data=/var/lib/elasticsearch, path.logs=/var/log/elasticsearch, path.repo=notConfigured
	sudo /bin/systemctl daemon-reload
	sudo systemctl enable elasticsearch.service #sudo /bin/systemctl enable elasticsearch.service
	sudo systemctl start elasticsearch.service
	curl -XGET 'localhost:9200/?pretty'  #see if it is running
	sudo nano /etc/elasticsearch/elasticsearch.yml  #edit node.name and cluster.name if needed
	ls -l /var/log/elasticsearch  #check elastic log is created 
	echo $ES_HOME
	
	#register snapshot as shared file in elasticsearch
	curl -XPUT $ELASTIC_HOST'/_snapshot/'$ELASTIC_SNAPHOT -d '{
    	"type": "fs",
    	"settings": {
        	"location": "$ELASTIC_FS_PATH",
        	"compress": true,
        	"chunk_size": "10m"
    	}
	}'
	
	# 4.install kibana: https://www.elastic.co/guide/en/kibana/current/deb.html
	#앞에서 elastic에서 다 레포 세팅이 되어져 있으므로 pass
	sudo apt-get update 
	sudo apt-get install kibana
	#config 파일에서 어떤 elastic을가리킬지 정해줌, 없으면 localhost:9200: https://www.elastic.co/guide/en/kibana/current/settings.html
	sudo nano /etc/kibana/kibana.yml
	
	sudo /bin/systemctl daemon-reload
	sudo systemctl enable kibana.service
	sudo systemctl start kibana.service
	echo $KIBANA_HOME #/usr/share/kibana, port:5601, 
	
	
	# 5.install logstash 
	#앞에 elastic에서 설정이 되어있으므로 pass
	#logstash는 설치 디렉토리로 반드시 이동한다 #=extract.path, see: https://www.elastic.co/guide/en/logstash/current/dir-layout.html	
	cd $LOGSTASH_INSTALL_PATH #설치 디렉토리로 이동
	
	sudo apt-get update 
	sudo apt-get install logstash
	#plugin 설치 확인 # cd /opt/logstash/bin 
	./plugin list | grep beats #can you see this? logstash-input-beats
	./bin/logstash-plugin install logstash-input-beats # run the following command inside the logstash directory /opt/logstash or /usr/share/logstash
	./bin/logstash-plugin update logstash-input-beats
	
	#sudo cat <<EOF > /etc/logstash/conf.d/$LOGSTASH_CONF1   #cat <<EOF | grep 'b' | tee b.txt
	#....filter {}
	#....output {}
	#EOF
	sudo cp $PROJECTDIR'/'$INSTALLSCRIPTDIR'/elk_files/logstash-1.conf /etc/logstash/conf.d/'$LOGSTASH_CONF1
	sudo nano /etc/logstash/conf.d/$LOGSTASH_CONF1 #02-beats-input.conf
	
	#basic run & test
	#cd logstash-5.0.1
	#bin/logstash -e 'input { stdin { } } output { stdout {} }'
	bin/logstash -f $LOGSTASH_CONF1 --config.test_and_exit #first-pipeline.conf
	bin/logstash -f $LOGSTASH_CONF1 --config.reload.automatic
	bin/logstash -f $LOGSTASH_CONF1
	
	sudo systemctl enable logstash.service
	sudo systemctl start logstash.service
	
	
	# 6.install filebeat: https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html
	curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.1.1-amd64.deb
	sudo dpkg -i filebeat-5.1.1-amd64.deb
	#filebeat.yml 설정  #https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-index-pattern.html
	sudo cp $PROJECTDIR'/'$INSTALLSCRIPTDIR'/elk_files/filebeat.yml /etc/filebeat/filebeat.yml' 
	
	systemctl enable filebeat
	systemctl start filebeat # sudo /etc/init.d/filebeat start
	#mac 에서는 이렇게 시작해야한다 sudo ./filebeat -e -c filebeat.yml -d "publish"
	#test curl -XGET 'localhost:9200/logstash-$DATE/_search?pretty&q=
	
		
	
	# 일반 fs 백업 :  curl -XPUT 'http://localhost:9200/_snapshot/my_backup' -d '{ "type": "fs", "settings": { "location": "/mount/backups/my_backup","compress": true }}'
	# test snapshot backup SNAPSHOT=`date +%Y%m%d-%H%M%S` : curl -XPUT "localhost:9200/_snapshot/my_backup/$SNAPSHOT?wait_for_completion=true"
	
	
	# 9.bootup service start register with systemctl enable...& start the services
	# 10. lsit the services running and summary report
	
}



STEP5="5/6 gcloud sdk install"
function install_gcloud_sdk {
	#install python 2.7 : sudo apt-get install python2.7 python2.7-dev
	sudo apt-get install python #install python2 
	python3 --version
	python --version
	
	
	#install gcloud sdk
	export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
	echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee /etc/apt/sources.list.d/google-cloud-sdk.list
	curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
	sudo apt-get update 
	sudo apt-get install google-cloud-sdk
	#sudo apt-get install google-cloud-sdk-app-engine-java
	sudo gcloud init
	#Yes -login ...
	gcloud components update
	
	
	gsutil update
	
	#test gcloud storage connection , 리모트에서 mysql_db가 백업/리스토어되므로, 여기서는 별로 백업할것이 없음, 그러나 연결만 테스트 해본다  
	#bucket을 만들고 싶으면, gsutil mb -c STANDARD -l US gs://[BUCKET_NAME]
	gsutil ls -l gs://$GCLOUDSTORAGE_BUCKET #see more at https://cloud.google.com/storage/docs/quickstart-gsutil
	
	#install gStorage plugin for elastic snapshot backup 
	sudo bin/elasticsearch-plugin install repository-gcs
	echo 'using PUT _snapshot/my_gcs_repository_on_compute_engine......{  "type": "gcs",  "settings": {  "bucket": "my_bucket",  "service_account": "_default_"}}'
	# see more on setting service account as file: https://www.elastic.co/guide/en/elasticsearch/plugins/master/repository-gcs-usage.html
	
	
	
}

#set apache reverse proxy to access kibana ( db server) from public  - httpd, 
STEP1="6/6 apache install"
function install_apache {
	# 7.install apache reverse proxy for kibana : http://moxz.tk/2016/02/reverse-proxy-kibana-with-apache/
	APACHECONFFILE='/etc/apache2/sites-enabled/kibana-www.conf'
	cat /etc/apache2/sites-enabled/40-www.conf
	# <VirtualHost 10.0.0.2:8443> proxy ProxyRequests Off, ProxyPass / http://10.0.0.6:5601/,    ProxyPassReverse / https://10.0.0.6:5601/
	sudo nano $APACHECONFFILE 
}



