#!/bin/bash

##########  2/3  sub_install packages as function    #############

#######고쳐서 다시 쓰자 
STEP1="0/5: install full manual"
function install_full_manual {}

STEP1="1/5: install gcloud launcher image"
function install_gcloud_launcher_image {
	#https://docs.bitnami.com/google/apps/wordpress-multisite/

}



STEP1="1/5: install (L)A(M)P"
function install_apache_php {

	########아래의 1/2 대신에 click to google deploy(launcher) image를 사용하자  
	#https://docs.bitnami.com/google/apps/wordpress-multisite/
	###### 그리고 wp-cli로 plugin 및 theme들을 설치함 
	###### 대신 jq,curl등만 설치하면 됨 
	
	#https://cloud.google.com/compute/docs/tutorials/setting-up-lamp
	sudo apt-get install apache2
	echo "update apache2.conf with <Directory /var/www/html/> AllowOverride All </Directory>"
	sudo nano /etc/apache2/apache2.conf
	sudo apt-get install php5 libapache2-mod-php5
	sudo apt-get install php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc
	php -r 'echo "\n\nYour PHP installation is working fine.\n\n\n";' #check php
	sudo systemctl enable apache2.service 
	sudo a2enmod rewrite
	sudo apache2ctl configtest
	sudo systemctl start apache2.service #sudo /etc/init.d/apache2 restart
	sudo systemctl restart apache2
	
	sudo apt-get -y install jq
	sudo apt-get -y install curl
	
}

STEP2="2/5: install mysql server"
function install_mysql_server {  #only local
	echo "running .....install_mysql_server at local for testing, and make sure you use remote origin db server..."
	sudo apt-get -y install mysql-server
	sudo apt-get install php5-mysql php-pear
	# mysql_secure_installation ; which removes the test database and secures the server for production.
	sudo mysql_secure_installation
	which mysql
	mysql --version
	
	sudo mysql -u root -p1234 -e "CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
	sudo mysql -u root -p1234 -e "GRANT ALL ON wordpress.* TO 'wordpressuser'@'localhost' IDENTIFIED BY 'password';"
	sudo mysql -u root -p1234 -e "FLUSH PRIVILEGES;
	sudo mysql -u root -p1234 -e "EXIT;
	
	TEMPSTR="mysql-local userName:"$MYSQLUSERNAME",password:"$MYSQLUSERPASSWORD
	echo TEMPSTR
	lgo TEMPSTR
	
	#restart mysql server
	sudo systemctl enable mysqld
	sudo systemctl start mysqld
	
}

STEP3="3/5: install WP-MU"
function install_wp_mu {
	#https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lamp-on-ubuntu-16-04
	#https://www.digitalocean.com/community/tutorials/how-to-set-up-multiple-wordpress-sites-using-multisite
	cd /tmp
	curl -O https://wordpress.org/latest.tar.gz
	tar xzvf latest.tar.gz
	touch /tmp/wordpress/.htaccess
	chmod 660 /tmp/wordpress/.htaccess
	cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php
	mkdir /tmp/wordpress/wp-content/upgrade
	sudo cp -a /tmp/wordpress/. /var/www/html
	sudo chown -R admin:www-data /var/www/html
	sudo find /var/www/html -type d -exec chmod g+s {} \;
	sudo chmod g+w /var/www/html/wp-content
	sudo chmod -R g+w /var/www/html/wp-content/themes
	sudo chmod -R g+w /var/www/html/wp-content/plugins
	curl -s https://api.wordpress.org/secret-key/1.1/salt/
	read -p "define('AUTH_KEY',,..define('SECURE_AUTH_KEY',.define('LOGGED_IN_KEY',...."
	read -p "define('DB_NAME', 'wordpress'); define('DB_USER', 'wordpressuser'); define('DB_PASSWORD', 'password');define('FS_METHOD', 'direct'); define('DB_HOST', 'localhost'); define('DB_CHARSET', 'utf8'); define('DB_COLLATE', ''); define('WP_ALLOW_MULTISITE', true);" YESNO
	
	nano /var/www/html/wp-config.php  ##########################
	read -p "<Directory /var/www/> Options Indexes FollowSymLinks MultiViews AllowOverride All Order allow,deny allow from all </Directory>" YESNO
	sudo nano /etc/apache2/sites-enabled/000-default
	sudo systemctl restart apache2
	#type in browser: http://server_domain_or_IP
	sudo chown -R www-data /var/www/html
	
	
	#set up mail server : https://cloud.google.com/compute/docs/tutorials/sending-mail/
	#https://kx.cloudingenium.com/linux/ubuntu/send-emails-wordpress-linux-host/
	sudo apt-get install mailutils
	sudo apt-get install postfix
	sudo systemctl enable postfix.service
	sudo systemctl start postfix.service
	#test email
	echo "This is the body of the email" | mail -s "This is the subject line" user@example.com
	php -a
	mail ('admin@example.com', "Hello World!", "My email setup works!");
	exit ();
	#set up mail server using gmail
	sudo apt-get install libsasl2-2 libsasl2-modules sasl2-bin
	read -p "smtp_sasl_auth_enable = yes smtp_sasl_security_options = noplaintext noanonymous smtp_sasl_password_maps = hash:/etc/postfix/sasl_password" YESNO
	nano  /etc/postfix/main.cf  ######################
	read -p "smtp.gmail.com USERNAME@gmail.com:USERPASSWORD" YESNO
	nano /etc/postfix/sasl_password
	sudo chmod 600 /etc/postfix/sasl_password
 	sudo postmap hash:/etc/postfix/sasl_password 
 	sudo postmap /etc/postfix/sender_canonical
 	sudo /etc/init.d/postfix restart
	
	######http://<ip>/wp/wp-admin/install.php
	
	
	
	# Install from a local zip file - wp theme install ../my-theme.zip
	#install wp-cli  on mac: brew install wp-cli
	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
	chmod +x wp-cli.phar
	sudo mv wp-cli.phar /usr/local/bin/wp
	wp --info
	wp help  #https://wp-cli.org/commands/core/
	
	WORDPRESSDIR="~/Sites/dev/wordpress"
	mkdir ~/Sites/dev/wordpress
	cd ~/Sites/dev/wordpress
	wp core download
	wp core version
	#wp core update
	chmod 644 wp-config.php
	wp core config --dbname=wordpress --dbuser=root --dbpass=root #--dbhost=host.db
	wp core multisite-install --url=http://localhost:8888/dev/wordpress/ --title=WordPress --admin_user=superadmin --admin_password=password --admin_email=admin@kewtea.com
	#wp core install --url=http://localhost:8888/dev/wordpress/ --title=WordPress --admin_user=superadmin --admin_password=password --admin_email=admin@kewtea.com
	wp super-admin list
	wp super-admin add superadmin2 #wp super-admin remove superadmin2
	#wp user create bob bob@example.com --role=author, wp user update 123 --display_name=Mary --user_pass=marypass, wp user delete 123 --reassign=567 # reassign posts to user 567
	cd wp-content
	mkdir uploads
	chgrp web uploads/
	chmod 775 uploads/
	wp plugin status #wp plugin update jetpack
	wp theme status #wp theme update --all 
	wp theme install twentysixteen --activate
	
	#install plugin and themse using command line interface(WP-Cli)
	#jetpack, woocommerce, wp-cache, wp-member, Akismet, 
	#Domain Mapping(https://premium.wpmudev.org/project/domain-mapping/), backup, multi-language, yeo-google-analytics, site.xml, robot.txt,
	#W3 Total Cache or https://wordpress.org/plugins/wp-super-cache/, WP Mail SMTP
	wp plugin install woocommerce
	wp plugin activate woocommerce
	
	#back up db : wp db export backup.sql
	#for site in $(wp site list --field=url)
	#do
	#	wp plugin install wordpress-importer --url=$site --activate
	#done
	
	
}

STEP4="4/5: install gcloud sdk"
function install_gcloud {

}

STEP5="5/5: install etc"
function install_etc {

}