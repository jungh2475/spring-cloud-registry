'''
Created on Mar 2, 2017

@author: jungh
'''

import os  #shutil
import sys
import shutil


class db_manager:
    
    def __init__(self,name):
        self.name=name
    
    def backup(self,source,destination):
        pass
    
    def restore(self,source,destination):
        pass
    
    def transfer(self,src,dest):
        #같은 파일시스템안에서,..혹은 gStorage Bucket, ASW S3, SSH-remote
        #if: elif: else:
        if 'gsbucket' in dest:
            print('run gscopy')
        elif 'aws' in dest:  #만약에 문자열 안에 aws가 있는 경우는? s.find('safe') != -1 || <6 앞쪽에 이 단어가 있어야함 or s.find('',beg,end) 
            print('run aws transfer')
        else:
            self.copyLargefile(src,dest)
        pass
    
    
    def copyFile(self,src,dest):
        try:
            shutil.copy(src, dest)  #'/home/test.txt'        
        except shutil.Error as e: # eg. src and dest are the same file
            print('Error: %s' % e)
        except IOError as e: # eg. source or destination doesn't exist
            print('Error: %s' % e.strerror)
    
    def copyLargeFile(self, src, dest, buffer_size=16000):
        with open(src, 'rb') as fsrc:
            with open(dest, 'wb') as fdest:
                try:
                    shutil.copyfileobj(fsrc, fdest, buffer_size)
                except shutil.Error as e: # eg. src and dest are the same file
                    print('Error: %s' % e)
                except IOError as e: # eg. source or destination doesn't exist
                    print('Error: %s' % e.strerror)
                
    


class db_mysql_manager:
    
    def __init__(self,name):
        self.name=name
    
    def backup(self,source,destination):
        pass
    
    def restore(self,source,destination):
        pass
    
    def transfer(self,source,destination):
        pass
    


    

if __name__ == "__main__":
    print("hello")

