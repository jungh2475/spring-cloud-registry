#!/bin/bash
#!/usr/bin/env bash


#vagrant-ubunutu
sudo apt-get install virtualbox
sudo apt-get install vagrant
#vagrant mac osx
if ! type "brew" > /dev/null; then
  ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)";
fi
brew tap phinze/homebrew-cask && brew install brew-cask;
brew cask install vagrant; 
brew cask install virtualbox;

mkdir vagrant_getting_started
cd vagrant_getting_started
vagrant init hashicorp/precise64

#vagrant box add hashicorp/precise64
#vagrant box add precise32 http://files.vagrantup.com/precise32.box
#vagrant init

vagrant up #vagrant up --provider=aws
vagrant ssh

#Vagrant.configure("2") do |config|
	config.vm.box = "hashicorp/precise64"
	#config.vm.box_url = "http://files.vagrantup.com/precise64.box"
	
	config.vm.provision :shell, path: "bootstrap.sh"
	#config.vm.provision "shell", inline: "echo hello"
	config.vm.provision "file", source: "~/.gitconfig", destination: ".gitconfig"
	config.vm.network "public_network"
		#config.vm.network :forwarded_port, guest: 80, host: 4567
		#config.vm.network "public_network", bridge: "en1: Wi-Fi (AirPort)"
		#config.vm.network "public_network", ip: "192.168.0.17"
	
	config.vm.define "web", primary: true do |web|
    	web.vm.box = "apache"
	end

	config.vm.define "db" do |db|
    	db.vm.box = "mysql"
	end
#end

vagrant suspend
vagrant halt
vagrant destroy


#install apache
#!/usr/bin/env bash
apt-get update
apt-get install -y apache2
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi

#testing:  wget -qO- 127.0.0.1


#gcloud sdk-ubunutu
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb https://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install google-cloud-sdk
#https://cloud.google.com/sdk/
gcloud init

#gcloud python

git commit -am "Updating configuration"
git config credential.helper gcloud.sh
git remote add cloud https://source.developers.google.com/p/[YOUR_PROJECT_ID]/
git push cloud

#
startup-script.sh VIEW ON GITHUB

#
gcloud compute instances create my-app-instance \
    --image=debian-8 \
    --machine-type=g1-small \
    --scopes userinfo-email,cloud-platform \
    --metadata-from-file startup-script=gce/startup-script.sh \
    --zone us-central1-f \
    --tags http-server

gcloud compute instances get-serial-port-output my-app-instance --zone us-central1-f

gcloud compute firewall-rules create default-allow-http-8080 \
    --allow tcp:8080 \
    --source-ranges 0.0.0.0/0 \
    --target-tags http-server \
    --description "Allow port 8080 access to http-server"

gcloud compute instances list #http://[YOUR_INSTANCE_IP]:8080

#
pip install --upgrade google-api-python-client
from oauth2client.client import GoogleCredentials
credentials = GoogleCredentials.get_application_default()
from googleapiclient import discovery
compute = discovery.build('compute', 'v1', credentials=credentials)
def list_instances(compute, project, zone):
    result = compute.instances().list(project=project, zone=zone).execute()
    return result['items']
def create_instance(compute, project, zone, name, bucket):
    # Get the latest Debian Jessie image.
    image_response = compute.images().getFromFamily(
        project='debian-cloud', family='debian-8').execute()
    source_disk_image = image_response['selfLink']

    # Configure the machine
    machine_type = "zones/%s/machineTypes/n1-standard-1" % zone
    startup_script = open(
        os.path.join(
            os.path.dirname(__file__), 'startup-script.sh'), 'r').read()
    image_url = "http://storage.googleapis.com/gce-demo-input/photo.jpg"
    image_caption = "Ready for dessert?"

    config = {
        'name': name,
        'machineType': machine_type,

        # Specify the boot disk and the image to use as a source.
        'disks': [
            {
                'boot': True,
                'autoDelete': True,
                'initializeParams': {
                    'sourceImage': source_disk_image,
                }
            }
        ],

        # Specify a network interface with NAT to access the public
        # internet.
        'networkInterfaces': [{
            'network': 'global/networks/default',
            'accessConfigs': [
                {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT'}
            ]
        }],

        # Allow the instance to access cloud storage and logging.
        'serviceAccounts': [{
            'email': 'default',
            'scopes': [
                'https://www.googleapis.com/auth/devstorage.read_write',
                'https://www.googleapis.com/auth/logging.write'
            ]
        }],

        # Metadata is readable from the instance and allows you to
        # pass configuration from deployment scripts to instances.
        'metadata': {
            'items': [{
                # Startup script is automatically executed by the
                # instance upon startup.
                'key': 'startup-script',
                'value': startup_script
            }, {
                'key': 'url',
                'value': image_url
            }, {
                'key': 'text',
                'value': image_caption
            }, {
                'key': 'bucket',
                'value': bucket
            }]
        }
    }

    return compute.instances().insert(
        project=project,
        zone=zone,
        body=config).execute()

def delete_instance(compute, project, zone, name):
    return compute.instances().delete(
        project=project,
        zone=zone,
        instance=name).execute()
def wait_for_operation(compute, project, zone, operation):
    print('Waiting for operation to finish...')
    while True:
        result = compute.zoneOperations().get(
            project=project,
            zone=zone,
            operation=operation).execute()

        if result['status'] == 'DONE':
            print("done.")
            if 'error' in result:
                raise Exception(result['error'])
            return result

        time.sleep(1)


#gStorage
gsutil mb gs://[YOUR-BUCKET-NAME]
gsutil defacl set public-read gs://[YOUR-BUCKET-NAME]

#apache libcloud
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

cls = get_driver(Provider.RACKSPACE)
driver = cls('username', 'api key', region='iad')

sizes = driver.list_sizes()
images = driver.list_images()

size = [s for s in sizes if s.id == 'performance1-1'][0]
image = [i for i in images if 'Ubuntu 12.04' in i.name][0]

node = driver.create_node(name='libcloud', size=size, image=image)
print(node)

#gcloud-libcloud
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

ComputeEngine = get_driver(Provider.GCE)
# Note that the 'PEM file' argument can either be the JSON format or
# the P12 format.
driver = ComputeEngine('your_service_account_email', 'path_to_pem_file',
                       project='your_project_id')
