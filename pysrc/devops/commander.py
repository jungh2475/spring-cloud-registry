'''
Created on Mar 20, 2017


    - pexpect, pxssh
    - fabric

@author: jungh
'''

import pexpect
from pexpect import pxssh #http://pexpect.readthedocs.io/en/stable/api/pxssh.html
import getpass
from fabric.api import local #pip install fabric3
from fabric.api import run,env,execute,task

class CliCommander(object):
    '''
    classdocs
    '''


    def __init__(self, commandmap):
        '''
        Constructor
        '''
        self.commandmap=commandmap
        pass
    
    def setenv(self, config):
        pass
    
    def register(self, config):
        pass
    
    def unregister(self, config):
        pass
    
    def pxsshdemo(self, hostname, username, password):
        try:
            s = pxssh.pxssh() #pxssh.pxssh(options={"StrictHostKeyChecking": "no", "UserKnownHostsFile": "/dev/null"}
            #hostname = raw_input('hostname: ')
            #username = raw_input('username: ')
            #password = getpass.getpass('password: ')
            s.login(hostname, username, password)
            s.sendline('uptime')   # run a command
            s.prompt()             # match the prompt
            print(s.before)        # print everything before the prompt.
            s.sendline('ls -l')
            s.prompt()
            print(s.before)
            s.sendline('df')
            s.prompt()
            print(s.before)
            s.logout()
        except pxssh.ExceptionPxssh as e:
            print("pxssh failed on login.")
            print(e)    
    def pexpectdemo(self):
        child = pexpect.spawn ('ftp ftp.openbsd.org')
        child.expect ('Name .*: ')
        child.sendline ('anonymous')    
        #print child.before
    
    def run(self, command):
        local(self.commandmap[command])
        pass
        

if __name__ == "__main__":
    
    comMap={'install_mysql':'sudo apt-get install -y mysql-server'}
    cli=CliCommander(comMap)
    print(cli.run('install_mysql'))
    pass
            