'''
Created on Mar 20, 2017

install: pip install fabric3, (sudo apt-get install fabric3?)

load by : 이름이 fabfile이면 현재 directory에서 자동 load 아니면, fab -f fab_tasks.py
    add fabfile = fab_tasks.py to ~/.fabricrc.

example: fab my_cluster function1:var=xxx function2 function3....
        fab -H localhost,linuxbox host_type
        fab hostname_check
        fab demo2
        fab sudo_command:"apt-get install geany"

try:  fab --list

method안에, reboot, with cd('/var/www/myapp/'):
        sudo,
        run('tar xzf /tmp/assets.tgz'), prompt, put, get, http://docs.fabfile.org/en/1.13/api/core/operations.html
        prompt('Specify favorite dish: ', 'dish', default='spam & eggs')
        release = prompt('Please specify process nice level: ', key='nice', validate=int)

Removing sudo password prompt
    sudo visudo
    %hng ALL=(ALL) NOPASSWD: ALL #user hng belongs to hng group so it would be
    hng ALL=(ALL) NOPASSWD: ALL


ssh-keygen
    ssh-keygen
        press enter so the key goes to the default location
        press enter to leave passphrase empty
        press enter to confirm the empty passphrase
    ssh-copy-id simo@10.10.10.10
    ssh-copy-id hng@webserver.local
        where env.hosts=["simo@10.10.10.10", "webserver.local"]

env.parallel=True @parallel
        
@author: jungh
'''


#from fabric.api import env,run, local, settings, abort
from fabric.api import *
from fabric.api import execute
from fabric.contrib.console import confirm
import time
from fabric.network import disconnect_all
#접속할 host에 대한 접속정보

def local_cluster():
    env.user ='root'
    env.hosts=[
            '192.168.203.31',
            '192.168.203.32',
            '192.168.203.33',
            '192.168.203.34',
            '192.168.203.35'
                ] # list of server setting
    env.password = 'xxxxxx'

def kewtea_cluster():
    env.use_ssh_config = True
    env.user = 'admin'
    env.hosts = ['myhost1.com','myhost2.com']
    env.key_filename=['keyfile.pem']
    
    env.roledefs = {
        'dev': {
            'hosts': ['id@host'],
        }
    }
    pass

def dmalt_cluster():
    pass



'''
    def method annotations:  @hosts(my_hosts) +(as merge) @roles('role1')
'''
def demo():
    '''
        to-dos
        1)hostName, diskSize, get ps-list,....profiling report
        2)connection-network testing
        3)install basic sw packages : java, python, git
        4)upgrade, migration(transfer), cleansing&removal
    '''
    run('print("hihi")')
    pass

@task(alias='dm') #이것을 넣으면 다른 method들이 노출이 안됨..
def demo2(param):
    #print(param)
    run('hostname -I')
    pass
@task
def command(cmd):
    '''
        fab command:"hostname -I"
    '''
    run(cmd)  #sudo(cmd)
@task
def sudo_command(cmd):
    sudo(cmd)

@task
def hostname():
        run('uname -a')

@task
def hostname_check():
    run("hostname")

def install(package):
    sudo("apt-get -y install %s" % package)

def local_cmd():
    local("echo fabtest >> test.log")

        
def update():
    #with cd('/path/to/tomcat'):
    result=run('sudo apt-get update')
    time.sleep(5)
    #if result.failed and not confirm(" #from fabric.contrib.console import confirm
    if result.failed or not confirm("..."):
        abort("Aborting at user request.")
#def deploy():, run, put,get,....

                  
if __name__ == "__main__":
    local_cluster()
    # as library: http://docs.fabfile.org/en/1.13/usage/library.html, http://docs.fabfile.org/en/1.13/usage/execution.html
    #execute(do_something, hosts=hostlist)             #["username@host"]      
    host_list=['192.168.0.2']
    try:
        #equivalent to 'fab my_cluster demo demo2'
        execute(demo, hosts=host_list)
        execute(demo2,'ttt', hosts=host_list)
    finally:
        disconnect_all()
    pass