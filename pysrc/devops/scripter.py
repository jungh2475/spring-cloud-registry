'''
Created on Mar 20, 2017

@author: jungh
'''

#import jinja2
from jinja2 import Environment, FileSystemLoader #PackageLoader, Template 
#loader = FileSystemLoader('/path/to/templates'

'''
  config files
  apache: https://www.jeffgeerling.com/blog/apache-virtualhosts-with-ansible-and-jinja2
  vagrant: https://gist.github.com/shivanshukumar/8614927
  varnish
  (adduser, mysql, sourcelist,...)
  
  docker.io
'''

class FileTemplate(object):
    '''
    classdocs
    '''
    

    def __init__(self): #, params
        '''
        Constructor
        '''
        self.env=Environment(loader=FileSystemLoader('templates'))
        self.template=''
        
        
    def render(self, tempfile, input_vars, outputfilename):
        self.template = self.env.get_template(tempfile)
        output=self.template.render(input_vars)
        #Template('Hello {{ name }}!').stream(name='foo').dump('hello.html') 
        with open(outputfilename, 'w') as f:
            f.write(output)
    '''
        templateVars = { "title" : "Test Example",
                 "description" : "A simple inquiry of function." }
    '''

if __name__ == "__main__":
    
    ft=FileTemplate()
    inputs={ "title" : "Test Example","description" : "A simple inquiry of function." }
    ft.render('hello.txt', inputs, 'logs/out.txt') #ft.render('hello.txt',{ "title" : "Test Example","description" : "A simple inquiry of function." } , 'logs/out.txt')
    print("output file is created")
    
    '''
        python3 -m doctest -v doctest_simple.py
        """ Returns a * b.
            >>> my_function(2, 3)
            6
            
        """
        import doctest
        doctest.testmod()
        
        import unittest
        class FibonacciTest(unittest.TestCase):
            def testCalculation(self):
                self.assertEqual(fib(0), 0)
        unittest.main()    
    '''
    