'''
Created on Mar 2, 2017

@author: jungh
'''


# Import flask and template operators
from flask import Flask, render_template
from datetime import datetime

# Import SQLAlchemy
#from flask.ext.sqlalchemy import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy
# Define the WSGI application object
app = Flask(__name__)

# Configurations
#app.config.from_object('config')
app.config.from_pyfile('config.py')
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('controllers/templates/404.html'), 404

# Import a module / component using its blueprint handler variable (mod_auth)
#from app.mod_auth.controllers import mod_auth as auth_module

# Register blueprint(s)
#app.register_blueprint(auth_module)
# app.register_blueprint(xyz_module)
# ..

# Build the database:
# This will create the database file using SQLAlchemy
db.create_all()


@app.route("/")
def template_test():
    return render_template('template.html', my_string="Wheeeee!", my_list=[0,1,2,3,4,5])


if __name__ == '__main__':
    app.run(debug=True)
    #app.run(host='0.0.0.0', port=8080, debug=True)