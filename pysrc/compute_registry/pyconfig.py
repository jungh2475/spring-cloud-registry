'''
Created on 2 Mar 2017

@author: junghlee
'''

import json

#jsonString = json.dumps(pyDict)
#type(jsonString) ..str
#pyDict = json.loads(jsonString)

#vagrantDic['create']

vagrantDic={
    'create':'vagrant up',  #build
    'restart':'vagrant up',  #start or restart
    'stop':'vagrant halt',
    'delete':'vagrant destroy'
}

dockerDic={
    'create':'',  #build
    'restart':'docker run',  #start or restart
    'stop':'',
    'delete':''
}

libCloudDic={
    'create':'',  #build
    'restart':'docker run',  #start or restart
    'stop':'',
    'delete':''
}

gComputeDic={
    'create':'',  #build
    'restart':'docker run',  #start or restart
    'stop':'',
    'delete':''
}

