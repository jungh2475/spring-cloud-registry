
#source : https://www.danpurdy.co.uk/web-development/raspberry-pi-kiosk-screen-tutorial/

 #user: pi
 change Password from raspbery to xxxx
 adduser
 setup wifi from command: sudo raspi-config, ifconfig
 get internet connection
 load script
 install sws
 register to dmalt server
 download assignment
 run tasks
 
 
 
tasks
 1) system status & task progress reporting
 2) install sw 
 5) kiosk with chromium browser -incognito -kiosk url
 3) crawling
 4) mediaplayer - pygame-or-musicplayer(FFmpeg audio), python mopidy(music), kodi(XMMP)
 6) iot measurement reporter (with arduino ble)
 
 ----------------
 turn-off screen save & sleep for slideshow
 sudo apt-get install x11-xserver-utils unclutter
 sudo apt-get install chromium x11-xserver-utils unclutter
 (network setting) sudo nano /etc/network/interfaces
 sudo raspi-config (enable ssh, change pi password)
 
 
 ......
 sudo reboot  #or shutdown -r now
 
######## install basic sw ##########

sudo apt-get install -y python3-pip
python3 --version
sudo apt-get install build-essential libssl-dev libffi-dev python-dev
sudo apt-get install -y python3-venv
#cd ......pyvenv venv01....source venv01/bin/activate
 
##################
 autostart
 --------
 /boot
 (pi user only)sudo nano /home/pi/.config/lxsession/LXDE-pi/autostart
 sudo nano /etc/xdg/lxsession/LXDE/autostart or
 (global) sudo nano /etc/xdg/lxsession/LXDE-pi/autostart (newer Raspbian image changed the folder “LXDE” to “LXDE-pi”)
 
 
 cd .config/lxsession/LXDE-pi/ (from user pi /home/pi)
 ---------
[autostart file]
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
#@xscreensaver -no-splash  #screensaver-off
@xset s off  #disables power management settings and stops the screen blanking after a period of inactivity
@xset -dpms
@xset s noblank
@sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium/Default/Preferences #prevent any error messages
@unclutter -idle 0.1 -root

@chromium --noerrdialogs --kiosk http://www.page-to.display --incognito

@/usr/bin/python /home/pi/example.py
@lxterminal
@leafpad

---------------
sudo apt-get install x11-xserver-utils unclutter  #hiding cursor and disable the screensaver
sudo nano /etc/lightdm/lightdm.conf
xserver-command= X -s 0 -dpms

autostart file
----------------
@lxpanel --profile LXDE
@pcmanfm --desktop --profile LXDE
@xscreensaver -no-splash

[media server]
https://opensource.com/life/16/3/make-music-raspberry-pi-milkytracker

 
[communicate with firefox browser]
sudo apt-get install python3
sudo apt-get install iceweasel
sudo apt-get install xvfb
pip3 install selenium
sudo pip install xvfbwrapper
download firefox driver for arm7: https://github.com/mozilla/geckodriver/releases/tag/v0.11.1
-cp to $PATH

create Firefox Profile : fireforx -ProfileManager
install r-kiosk extension in the profile

python
profile = webdriver.FirefoxProfile('path//Profiles/71v1uczn.default')
driver=webdriver.Firefox(profile)
driver...
driver.maximize_window()
driver.get("https://www.example.com/membersarea")
driver.close()

 
[communicate with chrome browser]
1.browser control
2.monitoring tv screen status - listening & watching via iot arduino or usb mic
selenium
USB microphone using alsamixer:-sudo modprobe snd_bcm2835, arecord -D plughw:1,0 test.wav, ctrl+c, aplay test.wav, alsamixer(sudo alsactl store)
or omxplayer -p -o hdmi /filepath/a.wav

[check the os version]
 cat /etc/os-release #PRETTY_NAME="Raspbian GNU/Linux 8 (jessie)"  #ubuntu: lsb_release -a #Description:Raspbian GNU/Linux 8.0 (jessie)
 uname -a #kernel version 
 
 
[display setting -auto]
 --------
 issue on monitor resolution auto-matching from command line(Default Rpi resolution is : 1366x768)
 disable_overscan=1 in /boot/config.txt
 
 

sudo edidparser edid # tvservice -d edid
DMI:EDID DMT mode (82) 1920x1080p @ 60 Hz with pixel clock 148 MHz has a score of 149416
nano /boot/config.txt (It isn't used by NOOBS)
hdmi_group=2
hdmi_mode=82 
 
 --------
 /boot/config.txt :
 http://www.youtube.com/watch?v=uRLYYWv5EVs
 overscan_left=24
overscan_right=24
Overscan_top=10
Overscan_bottom=24

Framebuffer_width=480
Framebuffer_height=320

Sdtv_mode=2
Sdtv_aspect=2

#wifi auto connection
sudo nano /etc/network/interfaces
auto lo

iface lo inet loopback
iface eth0 inet dhcp

allow-hotplug wlan0
auto wlan0


iface wlan0 inet dhcp
        wpa-ssid "ssid"
        wpa-psk "password"