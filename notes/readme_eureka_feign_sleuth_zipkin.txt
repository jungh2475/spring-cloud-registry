spring cloud
proxy: zuul
microservice management: eureka - feign (ribbon)
apm(app performance monitoring) : sleuth zipkin 
kong : api proxy server (rate limiter, cache, logging)
varnish
apache kafka?

########################
zuul - filter
source:
https://spring.io/guides/gs/routing-and-filtering/
http://www.baeldung.com/spring-rest-with-zuul-proxy
http://kerberosj.tistory.com/228

<groupId>org.springframework.cloud-><artifactId>spring-cloud-starter-zuul

@EnableZuulProxy@SpringBootApplication

application.properties
	zuul.routes.books.url=http://localhost:8090  //top domain proxy name localhost:8080/books =>그쪽은 spring.application.name=book, :8090
	ribbon.eureka.enabled=false
	server.port=8080

zuul:
  routes:
    foos:
      path: /foos/**
      url: http://localhost:8081/spring-zuul-foos-resource/foos

testing
	Response response = RestAssured.get("http://localhost:8080/foos/1");
    assertEquals(200, response.getStatusCode());

custom zuul filter
@Component
public class CustomZuulFilter extends ZuulFilter {
		RequestContext ctx = RequestContext.getCurrentContext();
        ctx.addZuulRequestHeader("Test", "TestSample"); 
        HttpServletRequest request = ctx.getRequest();
        return null;

###################################
eureka
<groupId>org.springframework.cloud<artifactId>spring-cloud-starter-eureka-server
@EnableEurekaServer

<artifactId>spring-cloud-starter-eureka
@EnableEurekaClient
application.properties
	eureka:
	  instance:
	    leaseRenewalIntervalInSeconds: 1
	    leaseExpirationDurationInSeconds: 2
	  client:
	    serviceUrl:
	      defaultZone: http://127.0.0.1:8761/eureka/
	    healthcheck:
	      enabled: true
	    lease:
	      duration: 5
 
	spring:
	  application:
	    name: customer-service
	    
###########################
rest vs java methods
GET /eureka/v2/apps/appID, POST /eureka/v2/apps/appID, PUT/DELETE /eureka/v2/apps/appID/instanceID,
PUT /eureka/v2/apps/appID/instanceID/status?value=OUT_OF_SERVICE
PUT /eureka/v2/apps/appID/instanceID/metadata?key=value

application.properties
	Application Name (eureka.name)
    Application Port (eureka.port)
    Virtual HostName (eureka.vipAddress)
    Eureka Service Urls (eureka.serviceUrls)
maven : <groupId>com.netflix.eureka<artifactId>eureka-client   

################################
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients

feign: api method mapper
source
http://howtoprogram.xyz/2016/07/18/java-rest-client-using-netflix-feign/
https://dzone.com/articles/the-netflix-stack-using-spring-boot-part-3-feign
https://jmnarloch.wordpress.com/2015/08/19/spring-cloud-designing-feign-client/
http://www.baeldung.com/intro-to-feign

<groupId>com.netflix.feign-><artifactId>feign-core, <artifactId>feign-jackson
model: @JacksonXmlRootElement(localName = "book")

import feign.Headers;
import feign.Param;
import feign.RequestLine;
 
@Headers("Accept: application/json")
public interface BookResourceFeign {
 
  @RequestLine("GET /v1/books")
  List<Book> getAllBooks();
 
  @Headers("Content-Type: application/json")
  @RequestLine("POST /v1/books")
  Book createBook(Book book);
 
  @Headers("Content-Type: application/json")
  @RequestLine("PUT /v1/books/{id}")
  Book updateBook(@Param("id") Long id, Book book);
 
  @RequestLine("DELETE /v1/books/{id}")
  void deleteBook(@Param("id") Long id);
 
}


BookResourceFeign bookResource = Feign.builder().encoder(new JacksonEncoder())
        .decoder(new JacksonDecoder()).target(BookResourceFeign.class, URI_BOOK);
bookResource.getAllBooks();    
or 
@Autowired
    private CustomerServiceFeignClient customerServiceFeignClient;

###################
compile 'org.springframework.cloud:spring-cloud-starter-feign'
@FeignClient("http://notification-service")  //or (name = "reddit-service", url = "${com.deswaef.reddit.url}", configuration = RedditFeignConfiguration.class)
public interface NotificationVersionResource {  
    @RequestMapping(value = "/version", method = GET)
    String version();
}


####################

public interface CrudClient<T> {  }
	@RequestMapping(method = RequestMethod.POST, value = "/") 
    long save(T entity);

@FeignClient(Authorization.SERVICE_ID)
public interface AccountClient {
 
    @RequestMapping(value = "/account",
            method = RequestMethod.GET,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            })
    ResponseEntity<Resource<UserInfoDTO>> getAccount();
}
##########################
public interface BookClient {
    @RequestLine("GET /{isbn}")
    BookResource findByIsbn(@Param("isbn") String isbn);
 
    @RequestLine("GET")
    List<BookResource> findAll();
 
    @RequestLine("POST")
    @Headers("Content-Type: application/json")
    void create(Book book);
}

BookClient bookClient = Feign.builder()
  .client(new OkHttpClient())  // or .client(RibbonClient.create())
  .encoder(new GsonEncoder())
  .decoder(new GsonDecoder())
  .logger(new Slf4jLogger(BookClient.class))
  .logLevel(Logger.Level.FULL)
  .target(BookClient.class, "http://localhost:8081/api/books");

실제 사용법 
Book book = bookClient.findByIsbn("0151072558").getBook();
    assertThat(book.getAuthor(), containsString("Orwell"));
    
    

#######################

#######################    
sleuth
http://ryanjbaxter.com/cloud/spring%20cloud/spring/2016/07/07/spring-cloud-sleuth.html

app_name, trace id, span id 
[slueth-sample,44462edc42f2ae73,44462edc42f2ae73,false]

<artifactId>spring-cloud-sleuth-zipkin</artifactId>
@Bean public AlwaysSampler defaultSampler() {


https://www.uila.com/
https://toyotechus.com/network-and-virtualization-solutions/cloud-datacenter-management/