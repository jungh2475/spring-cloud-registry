

######### log analysis

######## APM solutions
https://haydenjames.io/20-top-server-monitoring-application-performance-monitoring-apm-solutions/
https://blog.profitbricks.com/application-performance-management-tools/

http://munin-monitoring.org/ (opensource)
scouter :java only, http://www.slideshare.net/gunheelee923519/open-source-apm-scouter (agent, collector, client)
http://www.zabbix.com/  (opensource)
http://www.heliosdev.org/helios-apm
https://github.com/naver/pinpoint (java opensource,Pinpoint is comprised of 3 main components (Collector, Web, Agent), and uses HBase as its storage)

https://anturis.com/
http://www.boundary.com/
https://www.datadoghq.com/lpg/

######## apache (proxy)
Apache Response Time
httpd.conf

LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\" %D" combined_with_response_time
CustomLog /path/to/apache/logs/access_log combined_with_response_time

"%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" --%T/%D--" combined



_sourceCategory=Apache/Access
| parse regex "(?<microseconds>\d+)$"
| timeslice 1m
| microseconds/toLong(1000000) as seconds 
| avg(seconds) as response_time by _timeslice

sources: https://www.sumologic.com/apache/analyzing-response-time/


######## linux process 
top
cpuload
disk utils

curl response time
curl -s -w %{time_total}\\n -o /dev/null http://www.shellhacks.com
curl -s -w '\nLookup time:\t%{time_namelookup}\nConnect time:\t%{time_connect}\nPreXfer time:\t%{time_pretransfer}\nStartXfer time:\t%{time_starttransfer}\n\nTotal time:\t%{time_total}\n' -o /dev/null http://www.shellhacks.com
curl -s -w '\nLookup time:\t%{time_namelookup}\nConnect time:\t%{time_connect}\nAppCon time:\t%{time_appconnect}\nRedirect time:\t%{time_redirect}\nPreXfer time:\t%{time_pretransfer}\nStartXfer time:\t%{time_starttransfer}\n\nTotal time:\t%{time_total}\n' -o /dev/null http://www.shellhacks.com

######## mysql
mytop
mysqladmin 
mtop
innotop # for innodb

source: https://www.psce.com/blog/2015/01/22/tracking-mysql-query-history-in-long-running-transactions/
https://www.digitalocean.com/community/tutorials/how-to-use-mysql-query-profiling

/var/log/mysql/localhost-slow.log
mysqldumpslow -t 5 -s at /var/log/mysql/localhost-slow.log

source: (2011) http://yoshinorimatsunobu.blogspot.kr/2011/04/tracking-long-running-transactions-in.html

실제 측정이 아닌 시뮬레이션으로 알아보는 기능 
sudo mysqlslap --user=sysadmin --password --host=localhost  --auto-generate-sql --verbose
sudo mysqlslap --user=sysadmin --password --host=localhost  --concurrency=50 --iterations=10 --auto-generate-sql --verbose
sudo mysqlslap --user=sysadmin --password --host=localhost  --concurrency=50 --iterations=10 --create-schema=employees --query="SELECT * FROM dept_emp;" --verbose

######## network analytics
sudo tcpdump -i any -w /tmp/http.log &   # capture all and save to http.log
killall tcpdump
tcpdump -A -r /tmp/http.log | less

wireshark, netflow flow-tools

sudo apt-get install tcpflow # for ubuntu
tcpflow -p -c -i eth0 port 80  #inspect all HTTP requests



