

step1 using vbox+vagrant
vagrant init hashicorp/precise64
vagrant up
vagrant ssh
#logout -> vagrant halt(=shutdown) #destroy suspend(=sleep)

#check java/apt-get
add-apt-repository
java -version
sudo apt-get install -y python-software-properties #software-properties-common
sudo apt-get install -y software-properties-common
#sudo apt-get install -y python3-software-properties
sudo add-apt-repository ppa:webupd8team/java -y #oracle java for ubuntu
#Don't use ubuntu ppa's in Debian.
sudo add-apt-repository ppa:openjdk-r/ppa -y #openjdk java for ubuntu
#edit file at : sudo nano /etc/apt/sources.list
# list files at : grep . /etc/apt/sources.list.d/*
#ubuntu/debian version checking - lsb_release -a
sudo apt-get update
sudo apt-get install oracle-java8-installer
sudo apt-get install openjdk-8-jre-headless
sudo apt-get install -y openjdk-8-jdk

sudo update-alternatives --config java
sudo update-alternatives --config javac

java -version
echo $JAVA_HOME
sudo nano /etc/environment #for ubuntu, else sudo vim /etc/profile #
export JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")  #자동 추적장치 
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java"
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/" #JAVA_HOME="/usr/lib/jvm/open-jdk"
export PATH=$JAVA_HOME/bin:$PATH
source /etc/environment # source /etc/profile
######
mkdir -p ~/webProjects/elastic
cd ~/webProjects/elastic


####### using apt-get install elasticserach

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list
sudo apt-get update
# sudo nano /etc/apt/sources.list.d/elastic-5.x.list  #  중복이 없는지 확인 할것  
#/etc/apt/sources.list.d/elasticsearch-5.x.list for the duplicate entry or locate the duplicate entry amongst the files in /etc/apt/sources.list.d/ and the /etc/apt/sources.list file.
sudo apt-get install elasticsearch
ps -p 1 #check SysV init or systemd
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service
#sudo update-rc.d elasticsearch defaults 95 10
#sudo -i service elasticsearch start

#loggin journal setting;  --quiet option must be removed from the ExecStart command line in the elasticsearch.service file
sudo journalctl -f
sudo journalctl --unit elasticsearch --since  "2016-10-30 18:17:16"
sudo apt-get install curl
curl -XGET 'localhost:9200/?pretty'
curl http://127.0.0.1:9200/  #index.number_of_shards: 1, index.number_of_replicas: 0
curl -X POST 'http://localhost:9200/tutorial/helloworld/1' -d '{ "message": "Hello World!" }'
curl -X GET 'http://localhost:9200/tutorial/helloworld/1'

#There is insufficient memory for the Java Runtime Environment to continue. for 5.x
sudo nano /etc/elasticsearch/jvm.options  #-Xms200m -Xmx200m  # bin/ElasticSearch -Xmx=2G -Xms=2G
sudo nano /etc/default/elasticsearch #older version 2.x ES_HEAP_SIZE=300m

ps aux | grep elasticsearch
ps aux | grep 'elasticsearch'| awk '{print $2}'
ps -C  프로세스명  -o pid  --no-heading
ps -ef 1704 
kill -9 9893

####### file download
curl -L -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.2.0.tar.gz
tar -xvf elasticsearch-5.2.0.tar.gz
cd elasticsearch-5.2.0/bin
./elasticsearch &
#./elasticsearch -Ecluster.name=my_cluster_name -Enode.name=my_node_name


