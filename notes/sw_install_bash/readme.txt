여기는 VM(google cloud virtual machine -ubuntu 16x.)을 설치하고 관리(backup & run test)하는 스크립트들이 저장되어져 있다. 

	1. 설치 파일 목록
	2. VM 관리 스크립트 목록
	3. 처음 git으로 설치 하는 방법
	4. crontab 설정하여 주기적인 작업 설정하는 법 


############## 0. 전체 간략히 요약 :readmefirst.txt  ##################

[vm을 UTF-8.SSH에서 접속하는 경우]
이런식으로 접속한다 gcloud compute --project "jungh-1131-1131" ssh --zone "us-east1-d" "instance-1-spring" (keyphrase=k101100)
다음을 꼭 실행해 준다 
sudo locale-gen "en_US.UTF-8"
그리고 google: https://console.cloud.google.com/networking에 가서  kewtea(default) network에 맞는 firewall 정의를 내려준다
(그리고 만들때 내부 DB 서버이면 외부 IP가 안생기도록 세팅을 해준다 )

[A 새로 설치하는 경우] 
1.kew-vm-install-xxx.sh (전체 실행 파일  wrapper임, 설치하면서 ....xxx-log.txt, ...xxx-temp.txt을 같은 디렉토리에 만들게 됨)
2.kew-vm-install-xxx-config.txt (설정 기록 파일)
3.test-xxx-functions.sh (실재 부분별 설치 function  파일-설치전에 값들을 꼭 확인 할것)
란 세개의 파일이 꼭 있어야함. 부분적으로 설치하고 싶으면 functions.sh안의 명령어를 입력해도됨.

반드시 실행은 /User/myName/webProject/에서 mkdir로 공간을 만들고, 거기에서 스크립트가 있는 곳을 가르켜서 실행하는 것이 좋음.
(실행명령어: sudo /User/myName/webProject/kew-install/kew-vm-install-xxx.sh, 이 실행파일은 chmod +x로 실행가능하게 해 놓을것...)
다 만들고 나면 백업을 받을 수 있도록 google storage와의 연결/설정도 포함되어져 있음.

예시: 
sudo chmod +x ~/webProjects/kew-install-images/scripts/kew-vm-install-spring-mysql-elk-2016.sh
sudo /home/jungh/webProjects/kew-install-images/scripts/kew-vm-install-spring-mysql-elk-2016.sh


[B 설치후에 정기적인 백업을 설정 하는 경우]
crontab -e를 실행해서, 정기적인 백업이 되도록 한다 (메인 db 서버인 경우만 필요함. 
그외의 경우는 filebeat-logstash로 로그를 elasticsearch로 전달하는 세팅만 필요함:crontab이 아닌 service로 계속 돌리면 됨) 


################    1. 설치 파일 목록   ####################

git으로 이것을 가지고 가서 설치하면 된다 (맨 아래 가이드 참조)
두종류의 스크립트가 있다 

A) installation scripts
	- kew-vm-install-spring-mysql-elk-2016.sh  #including apache2
	- kew-vm-install-python-mysql-elk-2016.sh  #python machine learning, mysql, elastic포함 
	- kew-vm-install-wordpress_mu-2016.sh # multisite version using wp-cli

	(이 스크립트들은 별 기능은 없고, config 파일을 load하고,function file을 로드해서, 설치 과정을 log파일에 적는다)
	(설치하면서 자동 backup까지 하기 위해서는 gCloud Storage 계정 설정이 필요함. 이점의 skip할지 주의 할것) 

B) VM maintenance scripts
	- kew-vm-tools-2016.sh (정기적 자동 실행 파일 안내 포)
	(이 스크립트는 menu-select방식으로, config 파일을 loa하고, function file을 로드하고, 설치 과정을 log 파일에 적는다) 

	
	모든 sh파일들은 파일명-config.txt, (파일명)-function.sh, 파일명-log.txt, 파일명-temp.txt를 가지고 있다 
	가져가서 쓸려면 1)2)3)4)5)파일이 같이 이동해야함 

	수작업으로 각각 install해보고 싶으면 functions.sh 파일을 열어서 각각의 함수 method별로 실행해 볼수도 있다 
	주기적인 backup이 필요한 경우에는 crontab을 추가로 설치하여햐 함(문서 아래  참조 )


################    3. 처음 git으로 설치 하는 방법   ####################
------
git로 스크립트 가져 오기
GITDIR='/usr/local/tmp/kew-installs'
mkdir $GITDIR & cd $GITDIR 
git init
GITREMOTE='https://jungh2475@bitbucket.org/jungh2475/kew-images-docker.git'
git remote add origin $GITREMOTE
git pull origin master
------
가져온 스크립트를 실행 
	- 실행전에 sudo 혹은 su root
	- ~/webProjects로 이동하여 거기에서 ./kew-images/kew-vm-install-2016.sh 실행
	(실행전에 스크립트 폴더 안에 [@filename]-config.txt, install-functions.sh가 있는지 확인할것  

	
	
######### 4. readme.txt for crontab setting   #######################

주기적으로 하구 싶은 작업들을 먼져 선택하여, kew-vm-tools-functions에서 골라서 실행하면 된다.
argument를 넣어서 원하는 것을 실행한다
 
	주기적인 작업들
	- backup file 생성 및 gCloudStorage에 저장, 청소
	
crontab -l -u <user> #의심가는 사용자별로 다 나열? or cat /var/spool/cron/crontabs/<user>
crontab -e


line="* * * * * /path/to/command"
(crontab -u userhere -l; echo "$line" ) | crontab -u userhere -
(crontab -l -u unixite cat >>'EOF'
@daily /opt/scripts/cleanup.sh
### Here you can add as many crontab commands as you want
EOF ) | crontab -u unixite -


crontab -r  # 제거 remove
crontab -l -u unixite | grep -v rdate | crontab -u unixite -  #자동제거 

##################
1)crontab으로 1주일에 한번씩 혹은 매일 한번씩 들어가서 backup하기 ->그리고 이메일 보내기  
2)crontab으로 한시간에 한번씩 들어가서 동작하는 서비스들이 안 죽었는지 체크 하기 

1B)테스트 결과 이메일로 보내기 
3)문제가 있는 경우에 email을 보내거나, 다시 restart 시도 
->이 메일 보내는 것은 gcloud가 해당 포트를 막아서, gmail smtp를 사용하거나, vpn으로 ubuntu postix등을 relay해야하는데, 
대신에 간단히 elasticsearch에 직접 post하는 방법도 있다: curl -XPOST "http://localhost:9200/indexname/typename/optionalUniqueId" -d "{ \"field\" : \"value\"}"  



###########
원하는 부분만 출력해서 변수로 가져 오기 
ls -l | awk '/jungh/  {print $3 "-" $4}'
ls -l | awk '{print $9}' | awk ' NF > 0'   //file 이름들을 출력, 빈칸을 제거함 NF>0
awk '/UUID/' /etc/fstab  # UUDI 패턴이 들어 있는 모든 줄들을 출력함 





#############  5. eclipse에서 Shedded DLKT 모듈 설치해서 디버깅하기  #########

to debug in eclipse, please install the plugin 'Shelled' using DLKT
(help->install new software->from-eclipse-releases, filter 'dyn..' ->select ShellEd from programming languages in DLKT(dynamic language tool kits) 
check : http://www.eclipse.org/dltk/ (dynamic language tool kit)
https://codeyarns.com/2015/01/21/how-to-install-dltk-ruby-plugin-in-eclipse/
out-dated-source: http://thiagoleoncio.blogspot.kr/2015/02/how-to-run-shell-script-on-windows_21.html





############# 6. how to use - extended version  ############
(1/4) to install or build image disk
#############
1)copy .sh script and config file(installconfig.txt) template in your machine
2)configure 'configfile'
3)run ./install-xxx.sh (make-sure-before: chmod +x xxxfilename.sh)
4)ctrl-c to break. you can check the result on installlog.txt
(please remove other installxxx.sh not used to avoid accidental install)
5)optionally, run run-&-test procedures using ./kew-restart-backup-restore.sh
6)make sure rename with updated config file 
	kew-restart-backup-restore-[machineID].sh



###################
(2/4) to backup, restore, restart-test
###################
0)check the configfile is correct (e.g. gStorage name,....)
1)run ./kew-restart-backup-restore-[machineID].sh with arguments 
(or with no argument, select options from screen after run)
2)optionally, after backup or restore, run 'restart' or 'test' procedures


###################
(3/4)to update software (service)
###################
0)stop 
1)go to the SW folder and run git pull origin master(or Branch Name)
2)restart & test

